//
//  ViewController.swift
//  klok
//
//  Created by Private on 1/28/18.
//  Copyright © 2018 Private. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var someTimeLabel: UILabel!
    @IBOutlet weak var button: UIButton!
    
    var timer: Timer = Timer()
    
    @IBAction func getCurrentValue(_ sender: Any) {
        getCurrentDateAndTime()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        getCurrentDateAndTime()
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(getCurrentDateAndTime), userInfo: nil, repeats: true)
    }
    
    @objc func getCurrentDateAndTime() {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, MMMM, dd, yyy HH:mm:ss a"
        let str = formatter.string(from: Date())
        someTimeLabel.text = str
    }
}

